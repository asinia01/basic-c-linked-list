// Andrew M. Siniarski
// the dude abides
// linked list with structure project
// THE DUDES INVENTORY
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
using namespace std;

//declare function protos
void menu(int *);
void populate();
void display();
void onePart();
void addPart();
void modifyPart();
void deletePart();


//declare structure and first record
struct product
{
   int partNum;
   int quant;
   float price;
   struct product *next;
};
struct product *first;


int main()
{
    int choice;
    first = NULL;
    menu(&choice);
  
    do
    {  
      if(choice == 1)
      {
          system ("cls");
          populate();
       
      }   
      else if(choice == 2)
      {
          system ("cls");      
          display();     
       
      }
      else if(choice == 3)
      {
           system ("cls");
           onePart();     
      }
      else if(choice == 4)
           addPart();
      else if(choice == 5)
      {
           system ("cls");
           modifyPart();
      }
      else if(choice == 6)
      {
           system ("cls");
           deletePart();
      }
      else if(choice == 7)
      {
          system ("cls");
          printf("Peace man");
          break;
      }
      else
      {
          system("cls");
          printf("Pick a number between 1 and 7 man");
          printf("Press any key to continue");
          getch();
          break;
      }
        system("cls");
        menu(&choice);
     
    }while(choice>0 || choice<8);
    getch();
    return 0;  
}

void menu(int *choiceP)
{
//menu function**********************************************MENU
  
 do
 { 
 
     system("cls");  
     printf("\t\t\tTHE DUDE'S INVENTORY");
     printf("\n\t\t\t--------------------");
     printf("\n\n\n\tPlease select from the following options");
     printf("\n\n1.  Create an inventory with part number, quantity, and price");
     printf("\n2.  Print your inventory");
     printf("\n3.  Print the information for a specific part number");
     printf("\n4.  Add a new inventory item");
     printf("\n5.  Modify an existing item");
     printf("\n6.  Delete an item");
     printf("\n7.  Exit      ");
     scanf("%d", choiceP);
 
     if(*choiceP <1 || *choiceP >7)
         printf("C'mon man.  That's not a valid choice.  Try again");
 }while(*choiceP <1 || *choiceP >7);   
 

}
//POPULATE THE LIST     ***************************************************** POPULATE THE LIST
void populate()
{
    //add parts and stuff  
     
     char choice;   
        
     product *prev, *newAddr;
     
      do
     { 
     
    
     newAddr = (struct product *) malloc(sizeof(struct product));
     if(newAddr == NULL)
     {
         printf("Sorry man, no memory available at the moment.");
         exit(1);
     }
     
     if(first == NULL)
         first = newAddr;
     else    
         prev -> next = newAddr;    
         

    //system ("cls");
        
     do
     { 
     printf("\nEnter a part number      ");
     scanf("%d", &newAddr->partNum);
     if(newAddr->partNum < 1)
         printf("We need a positive, non zero number please");
     }while(newAddr->partNum < 1);
     printf("Enter the quantity     ");
     scanf("%d", &newAddr->quant);
     printf("Enter the price      ");
     scanf("%f", &newAddr->price);
     newAddr -> next = NULL;
     prev = newAddr;
   
     
     printf("Would you like to add another (y)es or (n)o?        ");
     scanf("\n%c", &choice);
     if(choice != 'Y' && choice != 'y' && choice != 'N' && choice != 'n')
     {
        printf("Seriously?  It's a (y)es or (n)o.  Try again");
        continue;
     }
     }while(choice == 'Y' || choice == 'y'); 
     
     system("cls");
}
//  DISPLAY THE WHOLE LIST *********************************************  DISPLAY THE WHOLE LIST
void display()
{
        //display whole list
      product *newAddr;
      
      
      newAddr = first;
      
  
      
      printf("\t\t\tTHE DUDE'S INVENTORY LIST");
      printf("\n\t\t\t-------------------------");
      printf("\n\n\nPart Number\t\tQuantity\t\tPrice");
      printf("\n------------------------------------------------------");
   
      
      while(newAddr != NULL)
      {
         printf("\n%d\t\t\t%d\t\t\t$%.2f", newAddr->partNum, newAddr->quant, newAddr->price);
         newAddr = newAddr -> next;
      }  
      printf("\nPress any key to continue");
      getch();
      system ("cls");
             
        
}
//  DISPLAY ONE PART*********************************************************DISPLAY ONE PART
void onePart()
{
     int num;
     product *newAddr;
     newAddr = first;
     system ("cls");
     printf("What part would you like to display?            ");
     do
     { 
     printf("\nEnter a part number      ");
     scanf("%d", &num);
     if(num < 1)
         printf("We need a positive, non zero number please");
     }while(num < 1);
            
     
     while(newAddr != NULL)
     {
         if(newAddr -> partNum == num)
         {
             system("cls");       
             printf("\t\t\tTHE DUDE'S INVENTORY LIST");
             printf("\n\t\t\t-------------------------");
             printf("\n\n\nPart Number\t\tQuantity\t\tPrice");
             printf("\n------------------------------------------------------");                    
             printf("\n%d\t\t\t%d\t\t\t$%.2f", newAddr->partNum, newAddr->quant, newAddr->price);
             break;
         }
        else if(newAddr->next == NULL)
         {
             printf("That part doesn't even exist man");
             break;
         }       
           newAddr = newAddr -> next;  
     } 
     
  
     printf("\nPress any key to continue");
     getch();    
  system ("cls");
}
//ADD ONE PART   ********************************************************* ADD ONE PART
void addPart()
{
          
     char choice;    
     product *prev, *newAddr, *current;
     
     do
     {
    
     newAddr = (struct product *) malloc(sizeof(struct product));
     if(newAddr == NULL)
     {
         printf("Sorry man, no memory available at the moment.");
         exit(1);
     }
     //set up current and previous for checking purposes
     current = first;    
     prev = first;    
         
     system ("cls");
      //verify positive number
     do
     { 
     printf("Enter a part number      ");
     scanf("%d", &newAddr->partNum);
     if(newAddr->partNum < 1)
         printf("We need a positive, non zero number please");
     }while(newAddr->partNum < 1);
     
     do
     {
         
                 
          //check to see if part already exists       
         if(newAddr -> partNum == current -> partNum)
         {
              printf("That part already exists man");
              break;
         }      
         //check to see if new part will be the first
         else if(current == first && (newAddr -> partNum < current -> partNum))
         {
    
             newAddr -> next = current;
             first = newAddr;
             printf("Enter the quantity  ");
             scanf("%d", &newAddr->quant);
             printf("Enter the price      ");
             scanf("%f", &newAddr->price);
             break;
         }
         //check to see if new part will fit between two existing
         else if(newAddr -> partNum < current -> partNum)
         {
             newAddr -> next = current;
             prev -> next = newAddr;  
             printf("Enter the quantity       ");
             scanf("%d", &newAddr->quant);
             printf("Enter the price      ");
             scanf("%f", &newAddr->price);
             break;                 
              
         }     
         //check to see if new part will be the last in the list     
         else if((newAddr -> partNum > current -> partNum) && (current -> next == NULL)/*newAddr -> partNum > current -> partNum*/)
         {
                  current -> next = newAddr;
                  newAddr -> next = NULL;
                  printf("Enter the quantity  ");
                  scanf("%d", &newAddr->quant);
                  printf("Enter the price      ");
                  scanf("%f", &newAddr->price);
                  break;

         }
             //increment current part, set current to previous
             prev = current; 
             current = current -> next;

     }while(current != NULL);

     
   
     

     printf("Would you like to add another (y)es or (n)o?     ");
     scanf("\n%c", &choice);
     if(choice != 'Y' && choice != 'y' && choice != 'N' && choice != 'n')
     {
        printf("Seriously?  It's a (y)es or (n)o.  Try again");
      
     }
    }while(choice == 'Y' || choice == 'y'); 
}

//MODIFY ONE PART    *********************************************  MODIFY ONE PART
void modifyPart()
{
     int num;
     product *newAddr;
     newAddr = first;

     do
     { 
     printf("\nEnter a part number to modify      ");
     scanf("%d", &num);
     if(num < 1)
         printf("\nWe need a positive, non zero number please");
     }while(num < 1);
     
     while(newAddr != NULL)
     {
         if(newAddr -> partNum == num)
         {
             printf("Enter the new quantity        ");
             scanf("%d", &newAddr->quant);
             printf("Enter the new price  ");
             scanf("%f", &newAddr->price);
             printf("\n%d\t\t\t%d\t\t\t$%.2f", newAddr->partNum, newAddr->quant, newAddr->price);
             break;
         }
        else if(newAddr->next == NULL)
         {
             printf("That part doesn't even exist man");
             break;
         }       
         newAddr = newAddr -> next;
     }
       printf("\nPress any key to continue");
       getch();
         system ("cls");  
}
//DELETE ONE PART  ******************************************************************    DELETE ONE PART
void deletePart()
{

     int num;
     char choice2;
     product *newAddr, *prev, *willy;
     newAddr = first;   
     prev = first;
     do
     { 
     printf("Enter a part number to delete     ");
     scanf("%d", &num);
     if(num < 1)
         printf("\nWe need a positive, non zero number please");
     }while(num < 1);
     
     while(newAddr != NULL)
     {
         if(newAddr -> partNum == num)
         {
             printf("\nThis record will be permanently deleted.\nAre you sure you want to continue?   ");
             scanf("\n%c", &choice2);
             
             if(choice2 == 'Y' || choice2 == 'y')
             {
                 if(newAddr == first)
                 {
                     first = newAddr -> next; 
                       
                     free(newAddr);
                     break;
                 }
                 else if(newAddr -> next != NULL)
                 {       
                     prev -> next = newAddr -> next;      
                     free(newAddr);
                     break;
                 }
                 else if(newAddr->next == NULL)
                 {
                     prev -> next = NULL;
                     free(newAddr);
                     break;
                 }
                 else
                 {
                     printf("That part doesn't even exist man");
                     break;
                 }
             }
             
             else
             {
                 main();
                 break;
             }

       
                           
         }    
         prev = newAddr;                
         newAddr = newAddr -> next;
     }
     printf("\nPress any key to continue");
     getch();
     system ("cls");
}
